import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {Observable} from "rxjs/Observable";
import {HttpClientModule} from '@angular/common/http';
import { AppComponent } from './app.component';
import { HttpComponentComponent } from './http-component/http-component.component';
import { RouterModule, Routes} from '@angular/router';
import { PostComponent } from './post/post.component';
import { IdComponent } from './post/id/id.component';

const routes: Routes = [
  { path: '', redirectTo: 'post', pathMatch: 'full' },
  { path: 'post', component: PostComponent },
  { path: 'post/:id', component: IdComponent},
  { path: '**', redirectTo: 'post' },

  


];


@NgModule({
  declarations: [
    AppComponent,
    HttpComponentComponent,
    PostComponent,
    IdComponent
    
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(routes)
     
      ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
