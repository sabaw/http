import { Component, OnInit, Output, Input } from '@angular/core';
import { HttpClient ,HttpErrorResponse} from '@angular/common/http';
import { Router, ActivatedRoute, Params, Data } from '@angular/router';
import {IId} from '../../id';
import { $ } from '../../../../node_modules/protractor';


@Component({
  selector: 'app-id',
  templateUrl: './id.component.html',
  styleUrls: ['./id.component.css']
})

export class IdComponent implements OnInit {
  
   DataResponse=[];
   found=false;

 ID :IId={id: 0}
  
  constructor(private http:HttpClient,private router : Router,private route : ActivatedRoute) { }
   
   
      
 
  OnBackClick(){
    this.router.navigate(['../post']);
  }
  ngOnInit() {
    this.ID.id = this.route.snapshot.params['id'];
    
    

    this.http.get(`http://jsonplaceholder.typicode.com/posts/${this.ID['id']}`)
    .subscribe((data:any)=>{
      console.log(`http://jsonplaceholder.typicode.com/posts/${this.ID['id']}`)
      console.log(data);
      this.DataResponse=data;
     
    })

  }
}